package net.eldiosantos.scala.pocs.fetcher

import net.eldiosantos.scala.pocs.fetcher.config.ConfigurationObject
import net.eldiosantos.scala.pocs.fetcher.domain.TargetObject
import net.eldiosantos.scala.pocs.relationalpersistence.config.repository.Database

/**
  * Created by Eldius on 11/01/2017.
  */
object MainFetcherObject extends App {
  val config = ConfigurationObject.config.start

  println("##################################################")
  println(s"howDeep: ${config.howDeep}")
  println("##################################################")
  println(s"url: ${config.url}")
  println("##################################################")

  val db = Database(ConfigurationObject.config.db)
  TargetObject(config.url, config.howDeep)
    .fetch()
    .map(p => {
      try {
        db.save(p)
      } catch {
        case e: Exception => {
          e.printStackTrace()
          null
        }
      }
    })
    .filter(p => p != null)
    .foreach(p => println(s"url: ${p.getUrl} [${p.getContent.size}]"))
}
