var mountains = List[Int](7, 13, 3, 5, 17, 24, 69, 33)



def max(list: List[Int]): Int = {
  def max(n: Int, list: List[Int]): Int = {
    if(list.nonEmpty) {
      if(n > list.head) {
        max(n, list.tail)
      } else {
        max(list.head, list.tail)
      }
    } else {
      n
    }
  }
  max(list.head, list.tail)
}

println(mountains.indexOf(max(mountains.reverse)))

