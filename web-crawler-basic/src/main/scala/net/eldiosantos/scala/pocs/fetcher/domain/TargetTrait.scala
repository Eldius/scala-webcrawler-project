package net.eldiosantos.scala.pocs.fetcher.domain

import java.net.URL

/**
  * Created by esjunior on 16/01/2017.
  */
trait TargetTrait {
  val url: String
  val howDeep: Int

  def fetch(): List[Page] = {
    val target = new URL(url)
    val page = Page(target)
    if(howDeep > 0) {
      page :: page.getLinks(howDeep - 1)
        .map(t => TargetObject.enhance(t))
        .flatMap(t => t.fetch())
        .toList
    } else {
      List(page)
    }
  }


}
