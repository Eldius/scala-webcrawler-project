package net.eldiosantos.scala.pocs.fetcher.domain

/**
  * Created by esjunior on 16/01/2017.
  */
object TargetObject {
  def apply(url: String, howDeep: Int) = new Target(url, howDeep) with TargetTrait
  def apply(url: String) = new Target(url, 0) with TargetTrait
  def enhance(target: Target) = this.apply(target.url, target.howDeep)
}
