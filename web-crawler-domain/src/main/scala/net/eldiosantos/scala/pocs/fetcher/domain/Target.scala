package net.eldiosantos.scala.pocs.fetcher.domain

import java.net.URL

/**
  * Created by esjunior on 16/01/2017.
  */
class Target(val url: String, val howDeep: Int) {
  def getUrl = url
  def getHowDeep = howDeep

  override def toString: String = {
    s"url: ${url}"
  }
}
