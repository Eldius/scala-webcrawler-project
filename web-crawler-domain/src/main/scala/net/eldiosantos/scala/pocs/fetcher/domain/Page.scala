package net.eldiosantos.scala.pocs.fetcher.domain

import java.net.URL

import org.jsoup.Jsoup

import scala.collection.JavaConverters

/**
  * Created by esjunior on 16/01/2017.
  */

class Page(val url: URL, val content: String) extends PageTrait {
  def getUrl = url
  def getContent = content
}

object Page {
  def apply(url: URL) = {
    try {
      new Page(
        url
        , Jsoup.connect(url.toString).ignoreContentType(true).ignoreHttpErrors(true).get().toString
      )
    } catch {
      case e: Exception => new Page(url, e.getMessage)
    }
  }

  def apply(url: String, content: String): Page = new Page(new URL(url), content)
}

trait PageTrait {
  val content: String
  val url: URL
  def getLinks(howDeep: Int): Set[Target] = {

    JavaConverters.asScalaIterator(Jsoup.parse(content).body().getElementsByTag("a").listIterator())
      .map(a => {
        val _url = a.attr("href")
        new Target(
          if(_url.startsWith("http")) _url
          else s"${url.getProtocol}://${url.getHost()}/${_url}"
          , howDeep
        )
      })
      .toSet
  }
}
