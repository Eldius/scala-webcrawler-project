package net.eldiosantos.scala.pocs.fetcher.akka.config

import com.typesafe.config.{Config, ConfigFactory}
import net.eldiosantos.scala.pocs.relationalpersistence.config.DatabaseConfig

/**
  * Created by esjunior on 13/01/2017.
  */
class Configuration(val config: Config) {

  // validate vs. reference.conf
  config.checkValid(ConfigFactory.defaultReference(), "simple-crawler")
  val rootConfig = config.getConfig("simple-crawler")

  // non-lazy fields, we want all exceptions at construct time
  val start = new StartConfig(rootConfig.getConfig("start"))
  val db = new DatabaseConfig(rootConfig.getConfig("database"))
}

class StartConfig(val config: Config) {
  val url = config.getString("url")
  val howDeep = config.getInt("howDeep")
}

object ConfigurationObject {
  val config = new Configuration(ConfigFactory.load())
}
