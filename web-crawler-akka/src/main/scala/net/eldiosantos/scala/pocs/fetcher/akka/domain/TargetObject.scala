package net.eldiosantos.scala.pocs.fetcher.akka.domain

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import net.eldiosantos.scala.pocs.fetcher.domain.{Page, Target}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

/**
  * Created by esjunior on 24/01/2017.
  */
object TargetObject {
  def apply(url: String, howDeep: Int) = new Target(url, howDeep) with TargetTrait
  def apply(url: String) = new Target(url, 0) with TargetTrait
  def enhance(target: Target) = this.apply(target.url, target.howDeep)
}
