package net.eldiosantos.scala.pocs.fetcher.akka.domain

import net.eldiosantos.scala.pocs.fetcher.akka.actor.FetcherActor

/**
  * Created by esjunior on 24/01/2017.
  */
trait TargetTrait {
  val url: String
  val howDeep: Int

  def fetch(): Unit = {
    FetcherActor.fetch(this)
  }
}
