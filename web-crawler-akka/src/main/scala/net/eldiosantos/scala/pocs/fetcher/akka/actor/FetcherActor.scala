package net.eldiosantos.scala.pocs.fetcher.akka.actor

import java.net.URL

import akka.actor.{Actor, ActorSystem, Props}
import net.eldiosantos.scala.pocs.fetcher.akka.domain.{TargetObject, TargetTrait}
import net.eldiosantos.scala.pocs.fetcher.domain.Page
import org.slf4j.LoggerFactory

/**
  * Created by esjunior on 25/01/2017.
  */
class FetcherActor extends Actor {
  val logger = LoggerFactory.getLogger(getClass)
  override def receive: Receive = {
    case FetcherActor.Fetch(_target) => {
      logger.debug(s"fetching url '${_target.url}'")
      val page = savePage(Page(new URL(_target.url)))
      if(_target.howDeep > 0) {
        page.getLinks(_target.howDeep - 1)
          .map(t => TargetObject.enhance(t))
          .foreach(t => t.fetch())
      }
    }
  }

  private def savePage(page: Page) = {
    PersistenceActor.persist(page)
    page
  }
}

object FetcherActor {
  val system = ActorSystem("fetcher")
  val props = Props(classOf[FetcherActor])
  val actor = system.actorOf(props)
  case class Fetch(_target: TargetTrait)
  def fetch(_target: TargetTrait) = actor ! Fetch(_target)
}
