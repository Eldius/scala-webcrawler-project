package net.eldiosantos.scala.pocs.fetcher.akka

import net.eldiosantos.scala.pocs.fetcher.akka.config.ConfigurationObject
import net.eldiosantos.scala.pocs.fetcher.akka.domain.TargetObject
import net.eldiosantos.scala.pocs.relationalpersistence.config.repository.Database

object MainAkkaFetcherObject extends App {
  val config = ConfigurationObject.config.start

  println("##################################################")
  println(s"howDeep: ${config.howDeep}")
  println("##################################################")
  println(s"url: ${config.url}")
  println("##################################################")

  val db = Database(ConfigurationObject.config.db)
  TargetObject(config.url, config.howDeep)
    .fetch()
  db.list()
    .foreach(p => println(s"url: ${p.getUrl} [${p.getContent.size}]"))
}
