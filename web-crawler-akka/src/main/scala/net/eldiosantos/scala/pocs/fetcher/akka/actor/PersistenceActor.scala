package net.eldiosantos.scala.pocs.fetcher.akka.actor

import akka.actor.{Actor, ActorSystem, Props}
import net.eldiosantos.scala.pocs.fetcher.akka.actor.PersistenceActor.Persist
import net.eldiosantos.scala.pocs.fetcher.akka.config.ConfigurationObject
import net.eldiosantos.scala.pocs.fetcher.domain.Page
import net.eldiosantos.scala.pocs.relationalpersistence.config.repository.Database
import org.slf4j.LoggerFactory

/**
  * Created by esjunior on 25/01/2017.
  */
class PersistenceActor extends Actor {
  val logger = LoggerFactory.getLogger(getClass)
  val db = Database(ConfigurationObject.config.db)
  override def receive: Receive = {
    case Persist(page) => {
      try {
        logger.debug(s"saving page '${page.getUrl}'")
        db.saveOrUpdate(page)
      } catch {
        case e: Exception => logger.error(s"Error saving page ${page.getUrl}", e)
      }
    }
  }
}

object PersistenceActor {
  case class Persist(page: Page)
  val system = ActorSystem("fetcher")
  val props = Props(classOf[PersistenceActor])
  val actor = system.actorOf(props)
  def persist(_page: Page) = actor ! Persist(_page)
}

