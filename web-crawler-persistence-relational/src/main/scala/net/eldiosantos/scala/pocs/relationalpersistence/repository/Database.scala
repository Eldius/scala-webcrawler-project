package net.eldiosantos.scala.pocs.relationalpersistence.config.repository

import java.sql.DriverManager

import net.eldiosantos.scala.pocs.fetcher.domain.Page
import net.eldiosantos.scala.pocs.relationalpersistence.config.DatabaseConfig
import org.slf4j.LoggerFactory

/**
  * Created by esjunior on 13/01/2017.
  */
class Database(val dbConf: DatabaseConfig) {

  val logger = LoggerFactory.getLogger(getClass)

  Class.forName(dbConf.driver)
  val conn = DriverManager.getConnection(dbConf.url, dbConf.user, dbConf.pass)

  def save(page: Page): Page = {

    logger.info(s"inserting '${page.getUrl.toString}'")

    val st = conn.prepareStatement("insert into page_content (url, content) values (?, ?)")

    st.setString(1, page.getUrl.toString)
    st.setString(2, page.getContent)

    st.execute()

    conn.commit()

    page
  }

  def save(pages: List[Page]): List[Page] = {
    pages.map(save(_))
  }

  def list(): List[Page] = {
    val rs = conn.createStatement().executeQuery("select url, content from page_content")
    var results = List[Page]()
    while(rs.next()) {
      results = Page(rs.getString("url"), rs.getString("content")) :: results
    }
    results
  }

  def find(url: String): Page = {
    val st = conn.prepareStatement("select url, content from page_content where url = ?")

    st.setString(1, url)

    val rs = st.executeQuery()

    if(rs.next()) {
      Page(rs.getString(1), rs.getString(1))
    } else {
      null
    }
  }

  def update(page: Page): Page = {

    logger.info(s"updating '${page.getUrl.toString}'")

    val st = conn.prepareStatement("update page_content set content = ? where url = ?")

    st.setString(1, page.getContent)
    st.setString(2, page.getUrl.toString)

    val rs = st.executeUpdate()

    page
  }

  def saveOrUpdate(page: Page): Page = {
    if(find(page.getUrl.toString) != null) {
      update(page)
    } else {
      save(page)
    }
  }
}

object Database {
  var database: Database = null
  def apply(dbConf: DatabaseConfig): Database = {
    if(database == null) {
      database = new Database(dbConf)
      val st = database.conn.createStatement()
      st.addBatch(dbConf.create)
      st.executeBatch()
    }

    database
  }
}
