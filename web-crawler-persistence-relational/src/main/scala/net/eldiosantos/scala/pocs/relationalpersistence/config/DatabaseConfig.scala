
package net.eldiosantos.scala.pocs.relationalpersistence.config

import com.typesafe.config.Config

class DatabaseConfig(val config: Config) {
  val url = config.getString("url")
  val driver = config.getString("class")
  val user = config.getString("user")
  val pass = config.getString("pass")
  val create = config.getString("create")
}
